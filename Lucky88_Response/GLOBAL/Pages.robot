*** Settings ***
Library           RPA.Browser    run_on_failure=Nothing    timeout=30    implicit_wait=10
Resource          Locators.robot
Resource          Template.robot
Library           DateTime
Library           String
Library           OperatingSystem
Library           RPA.HTTP

*** Keywords ***
Go To Home Page
    My Open Headless Chrom Browser
    Login With Valid Data

Append Content To File
    [Arguments]    ${content}
    Append To File    ${path}    ${content}    encoding=UTF-8

Run Keyword When A Step Failed
    [Arguments]    ${content}
    Run Keyword If    '${KEYWORD STATUS}' == 'FAIL'    Run Keywords    Append Content To File    FAILED\n${content}
    ...    AND    Fail
    Close All Browsers

Login With Valid Data
    Input Text When Element Is Visible    css:input#txt-username    ${username}
    Input Text When Element Is Visible    css:input#txt-password    123456
    Click Element If Visible    css:form#frm-quick-login > .btn-custom--login
    Check_Alert_If_Login_Failed
    [Teardown]

Check_Response_For_Non_Https
    [Arguments]    ${xpath_verify}    ${type}
    ${count_ele}    Get WebElements    ${xpath_verify}
    FOR    ${x}    IN    @{count_ele}
        ${attribute}    Get Element Attribute    ${x}    ${type}
        ${uri}    Remove String    ${attribute}    o88userApp.verifyPlayGame    OK    (    )    '    '    ,    ${SPACE}    //    https:    http:
        ${base_url}    Evaluate    "//" in "${attribute}"
        Continue For Loop If    ${base_url} == False
        ${resp}    Run Keyword If    ${base_url}==True    Http Get    https://${uri}
        Run Keyword If    ${resp.status_code}!=200    Append Content To File    FAILED\n${attribute}\nStatus = ${resp.status_code} - Reason = ${resp.reason}\nElapsed Time = ${resp.elapsed.total_seconds()}\r\n
    END
    [Teardown]

Check_Response_For_Non_Http
    [Arguments]    ${xpath_verify}    ${type}
    ${count_ele}    Get WebElements    ${xpath_verify}
    FOR    ${x}    IN    @{count_ele}
        ${attribute}    Get Element Attribute    ${x}    ${type}
        ${uri}    Remove String    ${attribute}    o88userApp.verifyPlayGame    OK    (    )    '    '    ,    ${SPACE}    //    https:    http:
        ${base_url}    Evaluate    "//" in "${attribute}"
        Continue For Loop If    ${base_url} == False
        ${resp}    Run Keyword If    ${base_url}==True    Http Get    http://${uri}
        Run Keyword If    ${resp.status_code}!=200    Append Content To File    FAILED\n${attribute}\nStatus = ${resp.status_code} - Reason = ${resp.reason}\nElapsed Time = ${resp.elapsed.total_seconds()}\r\n
    END
    [Teardown]

Check_Response_For_Https
    [Arguments]    ${xpath_verify}    ${type}
    ${count_ele}    Get WebElements    ${xpath_verify}
    ${leng}    Get Length    ${count_ele}
    Log To Console    ${leng}
    FOR    ${x}    IN    @{count_ele}
        ${attribute}    Get Element Attribute    ${x}    ${type}
        ${base_url}    Evaluate    "http" in "${attribute}"
        Continue For Loop If    ${base_url} == False
        ${resp}    Run Keyword If    ${base_url}==True    Http Get    ${attribute}
        Run Keyword If    ${resp.status_code}!=200    Append Content To File    FAILED\n${attribute}\nStatus = ${resp.status_code} - Reason = ${resp.reason}\nElapsed Time = ${resp.elapsed.total_seconds()}\r\n
    END
    [Teardown]

Check_Response
    [Arguments]    ${xpath_verify}    ${type}
    ${count_ele}    Get WebElements    ${xpath_verify}
    FOR    ${x}    IN    @{count_ele}
        ${attribute}    Get Element Attribute    ${x}    ${type}
        ${resp}    Http Get    ${attribute}
        Run Keyword If    ${resp.status_code}!=200    Append Content To File    FAILED\n${attribute}\nStatus = ${resp.status_code} - Reason = ${resp.reason}\nElapsed Time = ${resp.elapsed.total_seconds()}\r\n
    END
    [Teardown]

Check_Response_For_Games
    Sleep    5s
    FOR    ${x}    IN RANGE    0    4
        Wait Until Keyword Succeeds    5x    5s    Select Frame    css:div#main \ iframe
        Wait Until Keyword Succeeds    5x    5s    Select Frame    css:iframe#frmGame
        Sleep    5s
        Execute Javascript    document.getElementsByClassName('fun_mask')[${x}].click();
        Sleep    2s
        @{list}    Get Window Handles
        Switch Window    ${list}[1]
        ${url}    Get Location
        ${status}    Http Get    ${url}
        Run Keyword If    ${status.status_code}!=200    Append Content To File    FAILED\n${url}\nStatus Code: ${status.status_code} - Reason: ${status.reason}\r\n
        Close Window
        Switch Window    MAIN
    END

Check_Response_For_Supplier_Virtual_Casino
    [Arguments]    ${supplier}
    Execute Javascript    document.querySelector("#MG002-all").click();
    Execute Javascript    ${supplier}
    Sleep    5s
    Execute Javascript    document.getElementsByClassName('fun_mask')[0].click();
    Sleep    2s
    @{list}    Get Window Handles
    Switch Window    ${list}[1]
    ${url}    Get Location
    ${status}    Http Get    ${url}
    Run Keyword If    ${status.status_code}!=200    Append Content To File    FAILED\n${url}\nStatus Code: ${status.status_code} - Reason: ${status.reason}\r\n

Check_Alert_If_Login_Failed
    ${alert}    Run Keyword And Return Status    Wait Until Element Is Visible    css:#alertMsg > div > div > div > div.modal-body > p    5s
    Run Keyword If    ${alert}==True    Run Keywords    Click Element    css:#alertMsg > div > div > div > div.modal-footer > button
    ...    AND    Click Element    css:form#frm-quick-login > .btn-custom--login

Check Saba Club
    Click Element If Visible    css:a[title='saba club']
    Sleep    3s
    @{list}    Get Window Handles
    Switch Window    ${list}[1]
    Check_Response    //iframe[@id='frmGame']    src
    [Teardown]    Run Keyword When A Step Failed    Saba Club${FAILED msg}

Check Esport
    Click Element If Visible    css:a[title='esport']
    Sleep    3s
    @{list}    Get Window Handles
    Switch Window    ${list}[1]
    ${url}    Get Location
    ${resp}    Http Get    ${url}
    Run Keyword If    ${resp.status_code}!=200    Append Content To File    FAILED\n${attribute}\nStatus = ${resp.status_code} - Reason = ${resp.reason}\nElapsed Time = ${resp.elapsed.total_seconds()}\r\n
    [Teardown]    Run Keyword When A Step Failed    Esport${FAILED msg}

Check Casino Ebet
    Click Element If Visible    css:a[title='Sòng bài trực tuyến']
    Wait For Condition    return document.readyState=='complete'
    Check_Response    //div[@id='lcEbet']/div/div[*]/a[@href]    href
    Check Casino    css:div#lcEbet > div > div:nth-of-type(4) > .casino-live__link    css:div#gameMainDiv > canvas
    [Teardown]    Run Keyword When A Step Failed    Casino Ebet${FAILED msg}

Check Casino Ezugi
    Click Element If Visible    css:a[title='Sòng bài trực tuyến']
    Wait For Condition    return document.readyState=='complete'
    Check_Response    //div[@id='lcEzugi']/div/div[*]/a[@href]    href
    Check Casino    css:div#lcEzugi > div > div:nth-of-type(4) > .casino-live__link    css:div:nth-of-type(1) > .table__black___1Vjom.table__table___2k7-q \ .null.table__sizing_container___1lmd6
    [Teardown]    Run Keyword When A Step Failed    Casino Ezugi${FAILED msg}

Check Casino Vivo
    Click Element If Visible    css:a[title='Sòng bài trực tuyến']
    Wait For Condition    return document.readyState=='complete'
    Check_Response    //div[@id='lcVivo']/div/div[*]/a[@href]    href
    Check Casino    css:div#lcVivo > div > div:nth-of-type(4) > .casino-live__link    css:li#menu-nav-roulette > .toolbar-button-background.toolbar-option
    [Teardown]    Run Keyword When A Step Failed    Casino Vivo${FAILED msg}

Check Casino
    [Arguments]    ${casino}    ${verify_ele}
    Click Element If Visible    ${casino}
    Switch Window    NEW
    Wait Until Page Contains Element    ${verify_ele}

Check Casino AllBet
    Click Element If Visible    css:a[title='Sòng bài trực tuyến']
    Wait For Condition    return document.readyState=='complete'
    Check_Response    //div[@id='lcAg']/div/div[*]/a[@href]    href
    [Teardown]    Run Keyword When A Step Failed    Casino AllBet${FAILED msg}

Check GG Lottery
    Wait Until Page Contains Element    css:a#dropdownGateGame
    Scroll Element Into View    css:a#dropdownGateGame
    Click Element If Visible    css:div > a:nth-of-type(7)
    Wait For Condition    return document.readyState=='complete'
    Check_Response    //a[@href='http://lucky88.live/lobby/lode.aspx']    href
    [Teardown]    Run Keyword When A Step Failed    GG - Lottery${FAILED msg}

Check GG BanCa
    Wait Until Page Contains Element    css:a#dropdownGateGame
    Scroll Element Into View    css:a#dropdownGateGame
    Click Element If Visible    css:div > a:nth-of-type(6)
    Wait For Condition    return document.readyState=='complete'
    Check_Response    //a[@href='http://lucky88.live/lobby/banca.aspx']    href
    [Teardown]    Run Keyword When A Step Failed    GG - Ban Ca${FAILED msg}

Check GG NoHu QuaySo InGame
    Wait Until Page Contains Element    css:a#dropdownGateGame
    Scroll Element Into View    css:a#dropdownGateGame
    Click Element If Visible    css:li:nth-of-type(9) > div[role='menu'] > div > a:nth-of-type(3)
    Wait For Condition    return document.readyState=='complete'
    Check_Response    //div[@class="games__item"]//a[@href][@target]    href
    [Teardown]    Run Keyword When A Step Failed    GG - NoHu${FAILED msg}

Check GG Keno
    Wait Until Page Contains Element    css:a#dropdownGateGame
    Scroll Element Into View    css:a#dropdownGateGame
    Click Element If Visible    css:li:nth-of-type(9) > div[role='menu'] > div > a:nth-of-type(2)
    Wait For Condition    return document.readyState=='complete'
    Check_Response    //a[@href='http://lucky88.live/quayso/keno.aspx']    href
    [Teardown]    Run Keyword When A Step Failed    GG - Keno${FAILED msg}

Check GG NumberGame
    Wait Until Page Contains Element    css:a#dropdownGateGame
    Scroll Element Into View    css:a#dropdownGateGame
    Click Element If Visible    css:li:nth-of-type(9) > div[role='menu'] > div > a:nth-of-type(1)
    Wait For Condition    return document.readyState=='complete'
    Check_Response    //a[@href='http://lucky88.live/quayso/numbergame.aspx']    href
    [Teardown]    Run Keyword When A Step Failed    GG - Number Game${FAILED msg}

Check Ban Ca Hoang Gia
    Click Element If Visible    css:a[title='Bắn cá']
    Sleep    3s
    @{list}    Get Window Handles
    Switch Window    ${list}[1]
    Check_Response    //iframe[@id='frmGame']    src
    [Teardown]    Run Keyword When A Step Failed    Ban Ca Hoang Gia${FAILED msg}

Check Virtual Games
    Wait Until Page Contains Element    css:a#dropdownQucikGame
    Scroll Element Into View    css:a#dropdownQucikGame
    Click Element If Visible    css:div[role='menu'] \ a[title='virtual game']
    Sleep    3s
    @{list}    Get Window Handles
    Switch Window    ${list}[1]
    Wait Until Page Contains Element    css:iframe#frmGame
    Check_Response    //iframe[@id='frmGame']    src
    [Teardown]    Run Keyword When A Step Failed    Virtual Games${FAILED msg}

Check Table Games
    Wait Until Page Contains Element    css:a#dropdownQucikGame
    Scroll Element Into View    css:a#dropdownQucikGame
    Click Element If Visible    css:a[title='tablegame']
    Sleep    3s
    @{list}    Get Window Handles
    Switch Window    ${list}[1]
    Wait Until Page Contains Element    css:iframe#frmGame
    Check_Response    //iframe[@id='frmGame']    src
    [Teardown]    Run Keyword When A Step Failed    Table Games${FAILED msg}

Check Lottery
    Wait Until Page Contains Element    css:a#dropdownQucikGame
    Scroll Element Into View    css:a#dropdownQucikGame
    Click Element If Visible    css:li:nth-of-type(5) > div[role='menu'] > div > a:nth-of-type(3)
    Wait For Condition    return document.readyState=='complete'
    Check_Response    //*[@id="main"]/div/iframe    src
    [Teardown]    Run Keyword When A Step Failed    Lottery${FAILED msg}

Check Keno
    Wait Until Page Contains Element    css:a#dropdownQucikGame
    Click Element    css:a#dropdownQucikGame
    Click Element If Visible    css:a[title='Keno']
    Switch Window    NEW
    Wait For Condition    return document.readyState=='complete'
    Check_Response    //iframe[@id='frmGame']    src
    [Teardown]    Run Keyword When A Step Failed    Keno${FAILED msg}

Check NumberGame
    Wait Until Page Contains Element    css:a#dropdownQucikGame
    Scroll Element Into View    css:a#dropdownQucikGame
    Click Element If Visible    css:a[title='Number Game']
    Wait For Condition    return document.readyState=='complete'
    Check_Response    //*[@id="main"]/div/iframe    src
    [Teardown]    Run Keyword When A Step Failed    Number Game${FAILED msg}

Check Games Voidbridge
    Click Element If Visible    css:a[title='Trò chơi']
    Wait For Condition    return document.readyState=='complete'
    Wait Until Keyword Succeeds    5x    5s    Select Frame    css:div#main \ iframe
    Wait Until Keyword Succeeds    5x    5s    Select Frame    css:iframe#frmGame
    Check_Response_For_Supplier_Virtual_Casino    document.querySelector("#MG002-UG002_V12").click();
    [Teardown]    Run Keyword When A Step Failed    Games - Voidbridge${FAILED msg}

Check Games Spadegaming
    Click Element If Visible    css:a[title='Trò chơi']
    Wait For Condition    return document.readyState=='complete'
    Wait Until Keyword Succeeds    5x    5s    Select Frame    css:div#main \ iframe
    Wait Until Keyword Succeeds    5x    5s    Select Frame    css:iframe#frmGame
    Check_Response_For_Supplier_Virtual_Casino    document.querySelector("#MG002-UG002_V36").click();
    [Teardown]    Run Keyword When A Step Failed    Games - Spadegaming${FAILED msg}

Check Games Pragmatic Play
    Click Element If Visible    css:a[title='Trò chơi']
    Wait For Condition    return document.readyState=='complete'
    Wait Until Keyword Succeeds    5x    5s    Select Frame    css:div#main \ iframe
    Wait Until Keyword Succeeds    5x    5s    Select Frame    css:iframe#frmGame
    Check_Response_For_Supplier_Virtual_Casino    document.querySelector("#MG002-UG002_V32").click();
    [Teardown]    Run Keyword When A Step Failed    Games - Pragmatic Play${FAILED msg}

Check Games OneRNG
    Click Element If Visible    css:a[title='Trò chơi']
    Wait For Condition    return document.readyState=='complete'
    Wait Until Keyword Succeeds    5x    5s    Select Frame    css:div#main \ iframe
    Wait Until Keyword Succeeds    5x    5s    Select Frame    css:iframe#frmGame
    Check_Response_For_Supplier_Virtual_Casino    document.querySelector("#MG002-UG002_V10").click();
    [Teardown]    Run Keyword When A Step Failed    Games - OneRNG${FAILED msg}

Check Virtual Sport
    Click Element If Visible    css:.header-custom__virtual-sport > a > span
    Wait For Condition    return document.readyState=='complete'
    Check_Response    //iframe[@src='https://lucky88.win/virtualsports/load.aspx']    src
    [Teardown]    Run Keyword When A Step Failed    Virtual Sport${FAILED msg}

Check Games
    Click Element If Visible    css:a[title='Trò chơi']
    Wait For Condition    return document.readyState=='complete'
    Check_Response_For_Games
    [Teardown]    Run Keyword When A Step Failed    Games${FAILED msg}

Check Sports
    Click Element If Visible    css:a[title='Thể thao']
    Wait For Condition    return document.readyState=='complete'
    Check_Response    //div[@class='main-inner page clearfix']/iframe[1]    src
    [Teardown]    Run Keyword When A Step Failed    Sports${FAILED msg}

Register Random Account
    My Open Headless Chrom Browser
    Create File    ${path}
    Generate Random Account
    Input Random Register Data
    [Teardown]    Run Keyword When A Step Failed    Register${FAILED msg}

My Open Headless Chrom Browser
    Open Browser    ${HOMEPAGE URL}    chrome    options=add_argument("--disable-popup-blocking"); add_argument("--ignore-certificate-errors");add_argument("--disable-notifications");add_argument("--incognito");add_argument("--no-sandbox")
    Set Window Size    1920    1080

Generate Random Account
    ${gettime}    Get Current Date    result_format=%b%d%H%M%S
    Convert To String    ${gettime}
    Set Global Variable    ${gettime}
    ${getemail}    Get Current Date    result_format=%b%d%H%M%S@yandex.com
    Convert To String    ${getemail}
    Set Global Variable    ${getemail}

Input Random Register Data
    Click Element If Visible    css:form#frm-quick-login > .btn-custom--register
    Input Text When Element Is Visible    css:input#userName    ${gettime}
    Input Text When Element Is Visible    css:input#nameUser    cmxxzcxzc
    Input Text When Element Is Visible    css:input#pwd    123456
    Input Text When Element Is Visible    css:input#confirmPwd    123456
    Input Text When Element Is Visible    css:input#emailUser    ${getemail}
    Input Text When Element Is Visible    css:input#phoneUser    0123456789
    Select From List By Index    css:select#lucky_number    9
    Click Element If Visible    css:.row-submit .btn-default
    Wait Until Page Contains Element    css:div#isUser > .header-custom__username
